<?php

use App\Models\Layout;
use App\Models\Popup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('popup_layout_interactions', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('layout_id');
            $table->foreign('layout_id')->references('id')->on('layouts')->onDelete('cascade');


            $table->enum('action',config('panel.popup_interactions'));
            $table->enum('page_identifier',config('panel.page_identifiers'));
            $table->enum('device',config('panel.targeted_device_types'));


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('popup_layout_interactions');
    }
};

<?php

namespace App\Models;

use App\Http\Traits\DefaultOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PopupFormButton extends Model
{
    use HasFactory;

    /**
     * order the elements by id as desc
     */
    use DefaultOrder;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'popup_form_id',
        'value',
        'bg_color',
        'click_action_types',
        'click_action_value',
        'height',
        'width',
        'x_axis',
        'y_axis',
    ];


    public function Form()
    {
        return $this->belongsTo(PopupForm::class);
    }



}

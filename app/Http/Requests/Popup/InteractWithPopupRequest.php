<?php

namespace App\Http\Requests\Popup;

use App\Http\Traits\ResponseValidationFormRequest;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InteractWithPopupRequest extends FormRequest
{

    use ResponseValidationFormRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        return [
          'action' => ['required',Rule::in(config('panel.popup_interactions'))],
          'page_identifier' => ['required',Rule::in(config('panel.page_identifiers'))],
          'device' => ['required',Rule::in(config('panel.targeted_device_types'))],
        ];

    }



}

<?php

namespace App\Http\Resources;

use App\Http\Traits\PaginationResources;
use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{

    use PaginationResources;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */



    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'layout_id' => (int)$this->layout_id,
            'content' => $this->content,
            'height' => (int)$this->height,
            'width' => (int)$this->width,
            'x_axis' => (float)$this->x_axis,
            'y_axis' => (float)$this->y_axis,

            'layout' => new LayoutResource($this->whenLoaded('Layout'))


        ];

    }
}

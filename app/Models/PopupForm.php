<?php

namespace App\Models;

use App\Http\Traits\DefaultOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PopupForm extends Model
{
    use HasFactory;

    /**
     * order the elements by id as desc
     */
    use DefaultOrder;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'layout_id',
        'action_url',
        'height',
        'width',
        'x_axis',
        'y_axis',
    ];


    public function Layout()
    {
        return $this->belongsTo(Layout::class);
    }

    public function Inputs()
    {
        return $this->hasMany(PopupFormInput::class);
    }

    public function Buttons()
    {
        return $this->hasMany(PopupFormButton::class);
    }



}

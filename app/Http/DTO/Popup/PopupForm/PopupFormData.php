<?php


namespace App\Http\DTO\Popup\PopupForm;


use App\Http\DTO\Parents\ObjectData;
use Illuminate\Http\Request;

final class PopupFormData extends ObjectData
{
    public ?int       $id=null;
    public string     $action_url;
    public int        $height;
    public int        $width;
    public float      $x_axis;
    public float      $y_axis;

    /** @var array|array<PopupFormInputData */
    public array $form_inputs_data;
    /** @var array|array<PopupFormButtonData */
    public array $form_buttons_data;

    public static function fromRequest(Request $request): self
    {


        return new self([
            'action_url' => $request->action_url,
            'height' => (int)$request->height,
            'width' => (int)$request->width,
            'x_axis' => (float)$request->x_axis,
            'y_axis' => (float)$request->y_axis,

            'form_inputs_data' => PopupFormInputData::fromArray($request->inputs),
            'form_buttons_data' => PopupFormButtonData::fromArray($request->buttons),


        ]);
    }

    /**
     * @param $array
     * @return array|array<PopupFormData>
     */
    public static function fromArray($forms){

        $popupFormsData = [];

        foreach ($forms as $form){

            array_push(
                $popupFormsData,
                new self([
                    'action_url' => $form['action_url'],
                    'height' => (int)$form['height'],
                    'width' => (int)$form['width'],
                    'x_axis' => (float)$form['x_axis'],
                    'y_axis' => (float)$form['y_axis'],
                    'form_inputs_data' => PopupFormInputData::fromArray($form['inputs']),
                    'form_buttons_data' => PopupFormButtonData::fromArray($form['buttons']),
                ])
            );

        }
        return $popupFormsData;

    }


}

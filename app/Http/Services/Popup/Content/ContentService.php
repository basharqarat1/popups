<?php


namespace App\Http\Services\Popup\Content;


use App\Http\DTO\Popup\PopupContentData;
use App\Models\PopupContent;

class ContentService
{

    /**
     * @param PopupContentData $contentData
     * @param int $layout_id
     * @return  PopupContent
     */
    public static function create(PopupContentData $contentData,int $layout_id){
        return PopupContent::create(
            array_merge($contentData->all(),['layout_id'=>$layout_id])
        );
    }

}

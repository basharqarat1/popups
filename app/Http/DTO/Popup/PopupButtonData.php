<?php


namespace App\Http\DTO\Popup;


use App\Http\DTO\Parents\ObjectData;
use Illuminate\Http\Request;

final class PopupButtonData extends ObjectData
{
    public ?int       $id=null;
    public string     $value;
    public string     $bg_color;
    public string     $click_action_types;
    public string     $click_action_value;
    public int        $height;
    public int        $width;
    public float      $x_axis;
    public float      $y_axis;

    public static function fromRequest(Request $request): self
    {


        return new self([
            'value' => $request->value,
            'bg_color' => $request->bg_color,
            'click_action_types' => $request->click_action_types,
            'click_action_value' => $request->click_action_value,
            'height' => (int)$request->height,
            'width' => (int)$request->width,
            'x_axis' => (float)$request->x_axis,
            'y_axis' => (float)$request->y_axis,


        ]);
    }

    /**
     * @param $array
     * @return array|array<PopupButtonData>
     */
    public static function fromArray($buttons){

        $popupButtonsData = [];

        foreach ($buttons as $button){

            array_push(
                $popupButtonsData,
                new self([
                    'value' => $button['value'],
                    'bg_color' => $button['bg_color'],
                    'click_action_types' => $button['click_action_types'],
                    'click_action_value' => $button['click_action_value'],
                    'height' => (int)$button['height'],
                    'width' => (int)$button['width'],
                    'x_axis' => (float)$button['x_axis'],
                    'y_axis' => (float)$button['y_axis'],
                ])
            );

        }
        return $popupButtonsData;

    }


}

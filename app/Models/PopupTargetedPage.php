<?php

namespace App\Models;

use App\Http\Traits\DefaultOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Layout
 * which is describe the style of the popup card and the targeted platform
 * @package App\Models
 */
class PopupTargetedPage extends Model
{
    use HasFactory;

    /**
     * order the elements by id as desc
     */
    use DefaultOrder;



    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'popup_id',
        'page_identifier',
    ];



    public function Popup()
    {
        return $this->belongsTo(Popup::class);
    }




}

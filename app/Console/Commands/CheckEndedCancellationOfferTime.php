<?php

namespace App\Console\Commands;

use App\Http\Services\Paypal\PaypalService;
use App\Models\SubscriptionPlan;
use App\Models\UserSubscriptionPlan;
use Carbon\Carbon;
use Illuminate\Console\Command;

// This unused anymore delete it later
class CheckEndedCancellationOfferTime extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ended-cancellation-offer:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check ended subscription via cancellation offers and make it as basic subscription again';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // This unused anymore delete it later


        // TODO : we dont need this any more if we still using pan with with paid trail because the price wi be updated after the trail finished

        $compensationPlansIds = SubscriptionPlan::isCompensation()->pluck('id')->toArray();

        //get the active subscription by compensationPlans which will end this month and then update the subscription price and period to basic
        $userSubscriptionPlans = UserSubscriptionPlan::whereIn('subscription_plan_id',$compensationPlansIds)
            ->whereDate('end_date',Carbon::now()->addDay(25))
            ->isCanceled(false)
            ->get();

        $payPalService = new PaypalService();
        foreach ($userSubscriptionPlans as $userSubscriptionPlan){
            if($payPalService->isActiveSubscription($userSubscriptionPlan->payment_subscription_id)){
                $payPalService->updateSubscription($userSubscriptionPlan->payment_subscription_id);
            }
        }


        return Command::SUCCESS;
    }
}

<?php


namespace App\Http\Traits\DefaultScope;


use App\Scopes\isVerifiedScope;

trait DefaultVerified
{

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function bootDefaultVerified()
    {

        static::addGlobalScope(new isVerifiedScope());
    }

    public function scopeIgnoreVerifiedScope($query){
        return $query->withoutGlobalScope(isVerifiedScope::class);
    }

    public function scopeVerified($query,$verify=null){
        if(is_null($verify) || $verify===true){
            $verify = true;
        }else{
            $verify = false;
        }

        return $query->where('verified_status',$verify);
    }


    //Functions
    public function verify(){
        $this->update([
            'verified_status' => true
        ]);
    }

    public function unVerify(){
        $this->update([
            'verified_status' => false
        ]);
    }


}

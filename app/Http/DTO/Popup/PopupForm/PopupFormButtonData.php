<?php


namespace App\Http\DTO\Popup\PopupForm;


use App\Http\DTO\Parents\ObjectData;
use Illuminate\Http\Request;

final class PopupFormButtonData extends ObjectData
{
    public ?int       $id=null;
    public string     $value;
    public string     $bg_color;
    public string     $click_action_types;
    public string     $click_action_value;
    public int        $height;
    public int        $width;
    public float      $x_axis;
    public float      $y_axis;

    public static function fromRequest(Request $request): self
    {


        return new self([
            'value' => $request->value,
            'bg_color' => $request->bg_color,
            'click_action_types' => $request->click_action_types,
            'click_action_value' => $request->click_action_value,
            'height' => (int)$request->height,
            'width' => (int)$request->width,
            'x_axis' => (float)$request->x_axis,
            'y_axis' => (float)$request->y_axis,


        ]);
    }

    /**
     * @param $array
     * @return array|array<PopupFormButtonData>
     */
    public static function fromArray($formButtons){

        $popupFormButtonsData = [];

        foreach ($formButtons as $formButton){

            array_push(
                $popupFormButtonsData,
                new self([
                    'value' => $formButton['value'],
                    'bg_color' => $formButton['bg_color'],
                    'click_action_types' => $formButton['click_action_types'],
                    'click_action_value' => $formButton['click_action_value'],
                    'height' => (int)$formButton['height'],
                    'width' => (int)$formButton['width'],
                    'x_axis' => (float)$formButton['x_axis'],
                    'y_axis' => (float)$formButton['y_axis'],
                ])
            );

        }
        return $popupFormButtonsData;

    }


}

<?php

namespace App\Models;

use App\Http\Traits\DefaultOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Layout
 * which is describe the style of the popup card and the targeted platform
 * @package App\Models
 */
class Layout extends Model
{
    use HasFactory;

    /**
     * order the elements by id as desc
     */
    use DefaultOrder;



    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'popup_id',
        'type',
        'height',
        'width',
        'bg_color',
        'border_type',
        'border_color',
        'border_width',
        'shadow',
        'border_radius',
        'is_archived',
    ];


    //Relations
    public function Popup()
    {
        return $this->belongsTo(Popup::class);
    }


    public function PopupContents(){
        return $this->hasMany(PopupContent::class);
    }

    public function PopupButtons(){
        return $this->hasMany(PopupButton::class);
    }

    public function PopupForms(){
        return $this->hasMany(PopupForm::class);
    }

    public function PopupLayoutInteractions(){
        return $this->hasMany(PopupLayoutInteraction::class);
    }

    //Scopes
    public function scopeArchived($query,$status=false){
        return $query->where('is_archived',$status);
    }


    public function scopeDesktop($query){
        return $query->where('type',config('panel.targeted_device_types.desktop'));
    }


}

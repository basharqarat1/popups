<?php

use App\Http\Controllers\PopupAnalyticsController;
use App\Http\Controllers\PopupController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('popup/by-targeted-page',[PopupController::class,'getPopupsByTargetPageIdentifier']);
Route::post('popup',[PopupController::class,'store']);
Route::post('popup/{id}',[PopupController::class,'update']);

Route::get('popup/{id}/layout/{layout_id}',[PopupAnalyticsController::class,'getLayoutInteractionsStatistics']);
Route::post('popup/{id}/layout/{layout_id}',[PopupAnalyticsController::class,'interactWithPopupLayout']);

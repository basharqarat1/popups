<?php


namespace App\Http\DTO\Popup;


use App\Http\DTO\Parents\ObjectData;
use Illuminate\Http\Request;

final class PopupLayoutData extends ObjectData
{
    public ?int     $id=null;
    public int      $height;
    public int      $width;
    public string   $bg_color;
    public ?string  $border_type;
    public ?string  $border_color;
    public ?string  $border_width;
    public ?string  $shadow;
    public ?string  $border_radius;

    public static function fromRequest(Request $request): self
    {


        return new self(array(
            'height' => (int)$request->layout_height,
            'width' => (int)$request->layout_width,
            'bg_color' => $request->layout_bg_color,
            'border_type' => $request->layout_border_type,
            'border_color' => $request->layout_border_color,
            'border_width' => $request->layout_border_width,
            'shadow' => $request->layout_shadow,
            'border_radius' => $request->layout_border_radius,

        ));
    }


}

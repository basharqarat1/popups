<?php

namespace App\Models;

use App\Http\Traits\DefaultOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class PopupLayoutInteraction
 * which is describe the interactions on popups
 * @package App\Models
 */
class PopupLayoutInteraction extends Model
{
    use HasFactory;

    /**
     * order the elements by id as desc
     */
    use DefaultOrder;



    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'layout_id',
        'action',
        'page_identifier',
        'device',
    ];


    //Relations
    public function Layout()
    {
        return $this->belongsTo(Layout::class);
    }

    //Scopes
    public function scopeSubmitted($query){
        return $query->where('action',config('panel.popup_interactions.submitted'));
    }

    public function scopeClosed($query){
        return $query->where('action',config('panel.popup_interactions.closed'));
    }

    public function scopeClicked($query){
        return $query->where('action',config('panel.popup_interactions.clicked'));
    }

}

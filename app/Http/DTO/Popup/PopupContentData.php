<?php


namespace App\Http\DTO\Popup;


use App\Http\DTO\Parents\ObjectData;
use Illuminate\Http\Request;

final class PopupContentData extends ObjectData
{
    public ?int       $id=null;
    public string     $content;
    public int        $height;
    public int        $width;
    public float      $x_axis;
    public float      $y_axis;

    public static function fromRequest(Request $request): self
    {


        return new self([
            'content' => $request->contentt,
            'height' => (int)$request->height,
            'width' => (int)$request->width,
            'x_axis' => (float)$request->x_axis,
            'y_axis' => (float)$request->y_axis,


        ]);
    }

    /**
     * @param $array
     * @return array|array<PopupContentData>
     */
    public static function fromArray($contents){

        $popupContentsData = [];

        foreach ($contents as $content){

            array_push(
                $popupContentsData,
                new self([
                    'content' => $content['contentt'],
                    'height' => (int)$content['height'],
                    'width' => (int)$content['width'],
                    'x_axis' => (float)$content['x_axis'],
                    'y_axis' => (float)$content['y_axis'],
                ])
            );

        }
        return $popupContentsData;

    }


}

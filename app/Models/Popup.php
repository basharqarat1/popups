<?php

namespace App\Models;

use App\Http\Traits\DefaultOrder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Popup extends Model
{
    use HasFactory;

    /**
     * order the elements by id as desc
     */
    use DefaultOrder;


    const FULL_SCREEN_OVERLAYS = 'full_screen_overlays';
    const SLIDE_IN = 'slide_in';
    const EXIT_INTENT = 'exit_intent';
    const TYPES = [
      self::FULL_SCREEN_OVERLAYS => self::FULL_SCREEN_OVERLAYS,
      self::SLIDE_IN => self::SLIDE_IN,
      self::EXIT_INTENT => self::EXIT_INTENT,
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'type',
    ];


    //It hasMany relation because we maybe we have to layouts one for mobile ,desktop
    public function Layouts(){
        return $this->hasMany(Layout::class);
    }

    public function TargetedPages(){
        return $this->hasMany(PopupTargetedPage::class);
    }


}

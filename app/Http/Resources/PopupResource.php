<?php

namespace App\Http\Resources;

use App\Http\Traits\PaginationResources;
use Illuminate\Http\Resources\Json\JsonResource;

class PopupResource extends JsonResource
{

    use PaginationResources;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */



    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'title' => $this->title,
            'type' => $this->type,
            'created_at'=> $this->created_at->format('d-m-Y'),



            //return just last active layout because we dont delete or update it
            // we create new record to archive the old one with user interactions
            'layout' => new LayoutResource($this->whenLoaded('Layouts')->first()),

        ];

    }
}

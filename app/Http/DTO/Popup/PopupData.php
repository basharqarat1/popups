<?php


namespace App\Http\DTO\Popup;


use App\Http\DTO\Parents\ObjectData;
use App\Http\DTO\Popup\PopupForm\PopupFormData;
use App\Models\Popup;
use Illuminate\Http\Request;

final class PopupData extends ObjectData
{
    public ?int       $id=null;
    public ?string    $title;
    public ?string    $type;
    public PopupLayoutData    $layout_data;

    /** @var array|array<PopupContentData> */
    public array      $contents_data;
    /** @var array|array<PopupButtonData> */
    public array      $buttons_data;
    /** @var array|array<PopupFormData> */
    public array      $form_data;
    /** @var array|array<PopupTargetedPageData> */
    public array      $targeted_pages_data;

    public static function fromRequest(Request $request): self
    {


        return new self(array(
            'title' => $request->popup_title,
            'type' => $request->popup_type,

            'layout_data' => PopupLayoutData::fromRequest($request),
            'contents_data' => PopupContentData::fromArray($request->popup_contents),
            'buttons_data' => PopupButtonData::fromArray($request->popup_buttons),
            'form_data' => PopupFormData::fromArray($request->popup_forms),
            'targeted_pages_data' => PopupTargetedPageData::fromArray($request->pages_identifiers)


        ));
    }

    public static function forUpdate(Popup $popup,Request $request){
        return new self(array(
            'title' => $popup->title,
            'type' => $popup->type,

            'layout_data' => PopupLayoutData::fromRequest($request),
            'contents_data' => PopupContentData::fromArray($request->popup_contents),
            'buttons_data' => PopupButtonData::fromArray($request->popup_buttons),
            'form_data' => PopupFormData::fromArray($request->popup_forms),
            'targeted_pages_data' => PopupTargetedPageData::fromArray([])


        ));
    }


}

<?php

namespace App\Http\Resources;

use App\Http\Traits\PaginationResources;
use Illuminate\Http\Resources\Json\JsonResource;

class LayoutResource extends JsonResource
{

    use PaginationResources;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */



    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'popup_id' => (int)$this->popup_id,
            'type' => $this->type,
            'height' => (int)$this->height,
            'width' => (int)$this->width,
            'bg_color' => $this->bg_color,
            'border_type' => $this->border_type,
            'border_color' => $this->border_color,
            'border_width' => $this->border_width,
            'shadow' => $this->shadow,
            'border_radius' => $this->border_radius,
            'is_archived' => (bool)$this->is_archived,
            'created_at'=> $this->created_at->format('d-m-Y'),

            'popup' => new PopupResource($this->whenLoaded('Popup')),
            'contents' => ContentResource::collection($this->whenLoaded('PopupContents')),
            'buttons' => ButtonResource::collection($this->whenLoaded('PopupButtons')),
            'forms' => FormResource::collection($this->whenLoaded('PopupForms')),


        ];

    }
}

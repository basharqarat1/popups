<?php


namespace App\Http\Traits\DefaultScope;


use App\Scopes\isActiveScope;

trait DefaultActive
{

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function bootDefaultActive()
    {

        static::addGlobalScope(new isActiveScope());
    }

    public function scopeIgnoreActiveScope($query){
        return $query->withoutGlobalScope(isActiveScope::class);
    }

    public function scopeActive($query,$active=null){
        if(is_null($active) || $active===true){
            $active = true;
        }else{
            $active = false;
        }

        return $query->ignoreActiveScope()->where('is_active',$active);
    }


    //Functions
    public function activate(){
        $this->update([
            'is_active' => true
        ]);
    }

    public function deActivate(){
        $this->update([
            'is_active' => false
        ]);
    }


}

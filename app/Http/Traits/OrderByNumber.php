<?php


namespace App\Http\Traits;



use App\Models\Scopes\OrderByNumberScope;

trait OrderByNumber
{

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new OrderByNumberScope());
    }


}

<?php

namespace App\Providers;

use App\Models\UserInfo;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Auth::viaRequest('sso', function (Request $request) {
            return self::check_sso_token($request, false);
        });

        Auth::viaRequest('sso-admin', function (Request $request) {
            return self::check_sso_token($request, true);
        });
    }

    private static function check_sso_token($request, $admin)
    {
        $token = $request->bearerToken();
        if(!$token)
            return null;
        try{
            $t = explode('.',$token);
            if(count($t) != 3)
                return null;

            $token_info = base64_decode($t[1]);
            $json_token = json_decode($token_info);
            $issuer = env('SSO_HOST').'/realms/'.env('SSO_REALM');

            $sso_client_ids = explode(',',env('SSO_CLIENT_ID'));

            if($json_token->iss != $issuer)
                return null;
            if(!in_array($json_token->azp, $sso_client_ids))
                return null;

            $client = new Client();
            $url = $issuer . '/protocol/openid-connect/userinfo';
            $response = $client->request('GET', $url, [
                'headers' => [
                    'Authorization' => 'Bearer ' . $token,
                    'Accept' => 'application/json'
                ]
            ]);
            $data = json_decode($response->getBody(), true);

            if($admin && $data['role'] != 'admin')
                return null;

            $user = User::firstOrCreate(['uuid' => $data['sub']], [
                'profile_img' => null,
                'billing_vat_id' => null,
                'biography' => null,
                'billing_company' => null,
                'website' => null,
                'spotify' => null,
                'youtube' => null,
                'tiktok' => null,
                'instagram' => null,
                'facebook' => null,
                'total_points' => 0,
                'is_compensated' => 0,
                'watched_sessions' => 0,
                'billing_country' => null,
                'billing_city'    => null,
                'billing_address' => null,
                'billing_phone'   => null,
                'phone'   => null,
                'billing_zip'     => null,
                'inform_updates'  => true,
                'inform_products' => true,
                'billing_first_name' => $data['given_name']??'',
                'billing_last_name' => $data['family_name']??'',
                'billing_email' => $data['email']??'',
                'name' => $data['name']??'',
            ]);
            $user->first_name = $data['given_name']??'';
            $user->last_name = $data['family_name']??'';
            $user->type = $data['type']??'artist';
            $user->role = $data['role']??'user';
            $user->email = $data['email']??'';
            $user->email_verified = $data['email_verified']??false;
            $user->username = $data['preferred_username']??'';
            return $user;
        }
        catch(Exception)
        {
            return null;
        }
    }
}

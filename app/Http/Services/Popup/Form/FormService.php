<?php


namespace App\Http\Services\Popup\Form;



use App\Http\DTO\Popup\PopupForm\PopupFormData;
use App\Models\PopupForm;

class FormService
{

    /**
     * @param PopupFormData $formData
     * @param int $layout_id
     * @return  PopupForm
     */
    public static function create(PopupFormData $formData,int $layout_id){
        return PopupForm::create(
            array_merge($formData->all(),['layout_id'=>$layout_id])
        );
    }

}

<?php


namespace App\Http\DTO\Popup;


use App\Http\DTO\Parents\ObjectData;
use Illuminate\Http\Request;

final class PopupTargetedPageData extends ObjectData
{
    public ?int       $id=null;
    public string     $page_identifier;

    public static function fromRequest(Request $request): self
    {


        return new self(array(
            'page_identifier' => $request->page_identifier,
        ));
    }


    /**
     * @param $array $pageIdentifiers
     * @return array|array<PopupTargetedPageData>
     */
    public static function fromArray($pageIdentifiers){

        $popupTargetedPagesData = [];

        foreach ($pageIdentifiers as $pageIdentifier){

            array_push(
                $popupTargetedPagesData,
                new self([
                    'page_identifier' => $pageIdentifier,
                ])
            );

        }
        return $popupTargetedPagesData;

    }


}

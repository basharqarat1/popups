<?php


return[



    'site_languages'=>[
        'en' => 'en',
        'ar' => 'ar',
    ],

    'timezone' => 'UTC',

    'styles' => [
        'border_types' => [
            'none' => 'none',
            'solid' => 'solid',
            'dotted' => 'dotted',
            'dashed' => 'dashed'
        ]
    ],

    'targeted_device_types' => [
        'mobile' => 'mobile',
        'desktop' => 'desktop',
    ],

    'button_actions_types' => [
        'link' => 'link',
        'close' => 'close',
        'submit' => 'submit',
    ],

    'input_types' => [
        'text' => 'text',
        'number' => 'number',
        'email' => 'email'
    ],


    'page_identifiers' => [
        'homepage' => 'homepage',
        'about' => 'about',
        'login' => 'login',
    ],

    'popup_interactions' => [
        'submitted' => 'submitted',
        'clicked' => 'clicked',
        'closed' => 'closed',
    ]





];

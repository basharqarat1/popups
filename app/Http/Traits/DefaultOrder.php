<?php


namespace App\Http\Traits;



use App\Models\Scopes\DefaultOrderByScope;

trait DefaultOrder
{

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::addGlobalScope(new DefaultOrderByScope);
    }


}

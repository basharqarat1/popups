<?php


namespace App\Http\Services\Popup\Button;


use App\Http\DTO\Popup\PopupButtonData;
use App\Models\PopupButton;

class ButtonService
{

    /**
     * @param PopupButtonData $buttonData
     * @param int $layout_id
     * @return  PopupButton
     */
    public static function create(PopupButtonData $buttonData,int $layout_id){
        return PopupButton::create(
            array_merge($buttonData->all(),['layout_id'=>$layout_id])
        );
    }

}

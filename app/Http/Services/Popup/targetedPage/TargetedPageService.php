<?php


namespace App\Http\Services\Popup\targetedPage;


use App\Http\DTO\Popup\PopupLayoutData;
use App\Http\DTO\Popup\PopupTargetedPageData;
use App\Models\Layout;
use App\Models\PopupTargetedPage;

class TargetedPageService
{

    /**
     * @param PopupTargetedPageData $targetedPageData
     * @param int $popup_id
     * @return  PopupTargetedPage
     */
    public static function create(PopupTargetedPageData $targetedPageData,int $popup_id){
        return PopupTargetedPage::create(
            array_merge($targetedPageData->all(),['popup_id'=>$popup_id])
        );
    }

}

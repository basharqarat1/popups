<?php

namespace App\Exceptions;

use App\Http\Services\ApiResponse\ApiResponseClass;
use Exception;
use Throwable;

class ErrorMsgException extends Exception
{
    protected $message;

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        $this->message = $message;

        parent::__construct($this->message, $code, $previous);
    }

    /**
     * Report or log an exception.
     *
     * @return void
     */
    public function report()
    {

        /**
         * here we should print on our customized log channel
         */

    }

    public function render($request)
    {

        if(!empty($this->message))
            return ApiResponseClass::errorMsgResponse($this->message);
        return ApiResponseClass::errorMsgResponse();
    }
}

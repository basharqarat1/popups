<?php

namespace App\Http\Resources;

use App\Http\Traits\PaginationResources;
use Illuminate\Http\Resources\Json\JsonResource;

class ButtonResource extends JsonResource
{

    use PaginationResources;

    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */



    public function toArray($request): array
    {

        return [
            'id' => $this->id,
            'layout_id' => (int)$this->layout_id,
            'value' => $this->value,
            'bg_color' => $this->bg_color,
            'click_action_types' => $this->click_action_types,
            'click_action_value' => $this->click_action_value,
            'height' => (int)$this->height,
            'width' => (int)$this->width,
            'x_axis' => (float)$this->x_axis,
            'y_axis' => (float)$this->y_axis,

            'layout' => new LayoutResource($this->whenLoaded('Layout'))


        ];

    }
}

<?php


namespace App\Http\Services\Popup\Form;



use App\Http\DTO\Popup\PopupForm\PopupFormButtonData;
use App\Models\PopupFormButton;

class FormButtonService
{

    /**
     * @param PopupFormButtonData $formButtonData
     * @param int $form_id
     * @return  PopupFormButton
     */
    public static function create(PopupFormButtonData $formButtonData,int $form_id){
        return PopupFormButton::create(
            array_merge($formButtonData->all(),['popup_form_id'=>$form_id])
        );
    }

}

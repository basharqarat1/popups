<?php

namespace App\Http\Requests\Popup;

use App\Http\Traits\ResponseValidationFormRequest;
use App\Models\Popup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdatePopupRequest extends StorePopupRequest
{

    use ResponseValidationFormRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        return array_merge(
          $this->getLayoutValidationRules(),
          $this->getPopupContentValidationRules(),
          $this->getPopupButtonsValidationRules(),
          $this->getPopupFormsValidationRules()
        );

    }



}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PopupFormInput extends Model
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'popup_form_id',
        'type',
        'is_required',
//        'order',
        'width',
        'x_axis',
        'y_axis',
    ];


    public function Form()
    {
        return $this->belongsTo(PopupForm::class);
    }



}

<?php


namespace App\Http\Services\Popup\Form;



use App\Http\DTO\Popup\PopupForm\PopupFormInputData;
use App\Models\PopupFormInput;

class FormInputService
{

    /**
     * @param PopupFormInputData $formInputData
     * @param int $form_id
     * @return  PopupFormInput
     */
    public static function create(PopupFormInputData $formInputData,int $form_id){
        return PopupFormInput::create(
            array_merge($formInputData->all(),['popup_form_id'=>$form_id])
        );
    }

}

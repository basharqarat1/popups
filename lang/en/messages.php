<?php

return [

        'validation_error' => 'validation error',
        'successfully' => 'successfully',
        'deleted' => 'deleted',
        'not_found' => 'not found',
        'unAuthorized' => 'unauthorized',
        'something_went_wrong' => 'something went wrong',
];

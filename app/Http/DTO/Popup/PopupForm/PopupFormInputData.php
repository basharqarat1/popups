<?php


namespace App\Http\DTO\Popup\PopupForm;


use App\Http\DTO\Parents\ObjectData;
use Illuminate\Http\Request;

final class PopupFormInputData extends ObjectData
{
    public ?int       $id=null;
    public string     $type;
    public bool       $is_required;
    public int        $width;
    public float      $x_axis;
    public float      $y_axis;

    public static function fromRequest(Request $request): self
    {


        return new self([
            'type' => $request->type,
            'is_required' => (bool)$request->is_required,
            'width' => (int)$request->width,
            'x_axis' => (float)$request->x_axis,
            'y_axis' => (float)$request->y_axis,


        ]);
    }

    /**
     * @param $array
     * @return array|array<PopupFormInputData>
     */
    public static function fromArray($formInputs){

        $popupFormInputsData = [];

        foreach ($formInputs as $formInput){

            array_push(
                $popupFormInputsData,
                new self([
                    'type' => $formInput['type'],
                    'is_required' => (bool)$formInput['is_required'],
                    'width' => (int)$formInput['width'],
                    'x_axis' => (float)$formInput['x_axis'],
                    'y_axis' => (float)$formInput['y_axis'],
                ])
            );

        }
        return $popupFormInputsData;

    }


}

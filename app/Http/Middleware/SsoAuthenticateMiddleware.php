<?php

namespace App\Http\Middleware;

use App\Exceptions\ErrorMsgException;

use Closure;
use Illuminate\Http\Request;

class SsoAuthenticateMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
    }
}

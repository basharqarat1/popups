<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->uuid();
            $table->string('profile_img')->nullable();
            $table->string('company')->nullable();
            $table->string('utsid')->nullable();
            $table->text('biography')->nullable();
            $table->text('website')->nullable();
            $table->text('spotify')->nullable();
            $table->text('youtube')->nullable();
            $table->text('tiktok')->nullable();
            $table->text('instagram')->nullable();
            $table->text('facebook')->nullable();
            $table->integer('total_points')->default(0);
            $table->boolean('is_compensated')->default(0);
            $table->integer('watched_sessions')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};

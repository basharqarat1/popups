<?php

namespace App\Http\Middleware;

use App\Exceptions\ErrorMsgException;
use App\Http\Services\UserServices;
use Closure;
use Illuminate\Http\Request;

class PaginateProcessingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(!isset($request->per_page)){
            $request->merge([
                'per_page' => 10
            ]);
        }

        return $next($request);
    }
}

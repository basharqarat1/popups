<?php

namespace App\Http\Controllers;


use App\Http\Requests\Popup\InteractWithPopupRequest;
use App\Http\Services\ApiResponse\ApiResponseClass;
use App\Models\Layout;
use App\Models\Popup;
use App\Models\PopupLayoutInteraction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PopupAnalyticsController extends Controller
{


    /**
     * get the Statistics for defined popup and defined layout
     *
     * @param Request $request
     * @param $popup_id
     * @param $layout_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLayoutInteractionsStatistics(Request $request,$popup_id,$layout_id){

        $popupLayoutInteractionCount = PopupLayoutInteraction::where('layout_id',$layout_id)->count();

        $popupLayoutInteractionsByTypeCount = PopupLayoutInteraction::select('action',DB::raw('Count(*) as total'))
            ->where('layout_id',$layout_id)
            ->groupBy('action')
            ->get();

        //Initialize statistics array
        $statistics = [];
        foreach (config('panel.popup_interactions') as $popupInteractionType){
            $statistics["{$popupInteractionType}_percentage"] = 0;
        }

        //Fill the values
        foreach ($popupLayoutInteractionsByTypeCount as $popupLayoutInteraction){
            $statistics["{$popupLayoutInteraction->action}_percentage"] = round($popupLayoutInteraction->total/$popupLayoutInteractionCount * 100,2);
        }
        return ApiResponseClass::successResponse($statistics);


    }


    /**
     * get Statistics for all layouts in defined popup
     *
     * @param Request $request
     * @param $popup_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatisticsForDefinedPopubLayouts(Request $request,$popup_id){
        $popup = Popup::with(['layouts' => function ($query) {
                $query->withCount([
                    'PopupLayoutInteractions as clicked_count' => function ($query) {
                        $query->where('action', 'clicked');
                    },
                    'PopupLayoutInteractions as submitted_count' => function ($query) {
                        $query->where('action', 'submitted');
                    },
                    'PopupLayoutInteractions as closed_count' => function ($query) {
                        $query->where('action', 'closed');
                    }
                ]);
            }])
            ->findOrFail($popup_id);

        $statistics = [];

        $popupData = [
            'popup_id' => $popup->id,
            'layouts' => []
        ];

        $totalActionsForPopup = $popup->layouts->sum(function ($layout) {
            return $layout->clicked_count + $layout->submitted_count + $layout->closed_count;
        });

        foreach ($popup->layouts as $layout) {
            $layoutData = [
                'layout_id' => $layout->id,
                'clicked_percent' => $totalActionsForPopup ? ($layout->clicked_count / $totalActionsForPopup) * 100 : 0,
                'submit_percent' => $totalActionsForPopup ? ($layout->submitted_count / $totalActionsForPopup) * 100 : 0,
                'closed_percent' => $totalActionsForPopup ? ($layout->closed_count / $totalActionsForPopup) * 100 : 0
            ];

            $popupData['layouts'][] = $layoutData;
        }

        $statistics[] = $popupData;

        return ApiResponseClass::successResponse($statistics);
    }



    /**
     *
     * i wil use this function for A\B testing to record the action on the popup to analysis the clicked or closed count
     * and in which page and he device type
     *
     * @see PopupLayoutInteractionObserver
     *
     * @param InteractWithPopupRequest $request
     * @param $popup_id
     * @param $layout_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function interactWithPopupLayout(InteractWithPopupRequest $request,$popup_id,$layout_id){
        $layout = Layout::findOrFail($layout_id);

        PopupLayoutInteraction::create([
            'layout_id' => $layout->id,
            'action' => $request->action,
            'page_identifier' => $request->page_identifier,
            'device' => $request->device,
        ]);

        return ApiResponseClass::successMsgResponse();

    }

}

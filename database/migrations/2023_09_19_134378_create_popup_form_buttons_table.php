<?php

use App\Models\Layout;
use App\Models\Popup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('popup_form_buttons', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('popup_form_id');
            $table->foreign('popup_form_id')->references('id')->on('popup_forms')->onDelete('cascade');


            //the text wil appear in the button
            $table->string('value');
            $table->string('bg_color');
            $table->enum('click_action_types',[
                config('panel.button_actions_types.link'),
                config('panel.button_actions_types.submit'),
            ]);
            $table->string('click_action_value');
            $table->integer('height');
            $table->integer('width');
            //for display the element in the screen when need the position inside the form
            $table->float('x_axis');
            $table->float('y_axis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('popup_form_buttons');
    }
};

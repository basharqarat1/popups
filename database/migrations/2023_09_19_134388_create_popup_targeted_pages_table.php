<?php

use App\Models\Layout;
use App\Models\Popup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('popup_targeted_pages', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('popup_id');
            $table->foreign('popup_id')->references('id')->on('popups')->onDelete('cascade');


            //the page_identifier should be known and static for all website pages (between frontend & backend)
            $table->enum('page_identifier',config('panel.page_identifiers'));


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('popup_targeted_pages');
    }
};

<?php

namespace App\Console\Commands;

use App\Exceptions\ErrorMsgException;
use App\Http\Services\Paypal\PaypalService;
use App\Models\UserSubscriptionPlan;
use Carbon\Carbon;
use Illuminate\Console\Command;

class CheckEndedSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ended-subscription:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check ended subscription and cancel the ended and invalid subscriptions ';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        //This should run after 10:00 am because the payPa transaction its checking last check to renew at this time
        //so the correct status of subscription after it

        //TODO the end subscription should check on subscription after 10:00 am UTC because the renew on paypal will be at this time


        $userSubscriptionPlans = UserSubscriptionPlan::whereDate('end_date',Carbon::now())
//            ->isExpired(false)
            ->get();

        $payPalService = new PaypalService();
        foreach ($userSubscriptionPlans as $userSubscriptionPlan){
            $payPalSubscriptionDetails = $payPalService->getSubscriptionDetails($userSubscriptionPlan->payment_subscription_id);
            if ($payPalService->isActiveSubscription($payPalSubscriptionDetails['status']??'')) {
                UserSubscriptionPlan::create([
                    'user_id' => $userSubscriptionPlan->user_id,
                    'payment_subscription_id' => $userSubscriptionPlan->payment_subscription_id,
                    'subscription_plan_id' => $userSubscriptionPlan->subscription_plan_id,
                    'start_date' => Carbon::now(),
                    'end_date' => new Carbon($payPalSubscriptionDetails['billing_info']['next_billing_time']),
                ]);
            }else{
                $userSubscriptionPlan->update([
                    'is_expired' => true,
                ]);
            }
        }


        return Command::SUCCESS;
    }
}

<?php

use App\Http\Controllers\Admin\AdminCourseController;
use App\Http\Controllers\Admin\AdminSessionController;
use App\Http\Controllers\Admin\AdminStageController;
use App\Http\Controllers\Admin\AdminSubscriptionController;
use App\Http\Controllers\Admin\AdminTaskController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * @see \App\Providers\RouteServiceProvider boot()
 */

Route::options('{path}', function(){
    return response()->json(null, 204);
})->where('path', '.*');

Route::group(['middleware' => ['auth:sso-admin']],function(){

    //Route::get('stages/paginate',[AdminStageController::class,'paginate']);
    Route::get('stages/by-course/{course_id}',[AdminStageController::class,'getByCourseId']);


    Route::resource('tasks', AdminTaskController::class);

    Route::get('subscription', [AdminSubscriptionController::class, 'getAllSubscriptions']);

    Route::resource('sessions', AdminSessionController::class);
    //Route::get('sessions/paginate',[AdminSessionController::class,'paginate']);
    Route::get('sessions/by-stage/{stage_id}',[AdminSessionController::class,'getByStageId']);


    Route::resource('courses', AdminCourseController::class);
    //Route::get('courses/paginate',[AdminCourseController::class,'paginate']);


    Route::resource('stages', AdminStageController::class);
});

Route::group(['middleware' => ['guest:api']], function () {

});


<?php


namespace App\Http\DTO\Parents;
use App\Exceptions\ErrorMsgException;
use \Spatie\DataTransferObject\DataTransferObject;

use Carbon\Carbon;

class ObjectData extends DataTransferObject
{

    /**
     * @param array-key of strings
     * @return static
     * @throws  ErrorMsgException
     */
    public function merge(array $fields){
        foreach ($fields as $fieldName =>$field){
            if($this->checkPropertyExists($fieldName)){
                $this->{$fieldName} = $field;
            }
        }
        return $this;
    }

    /**
     * check if the property @param string $name is exists in the class variables
     * else @throws ErrorMsgException
     */
    private function checkPropertyExists($name){
        if(!property_exists($this, $name)){
            throw new ErrorMsgException(
                'you trying to sign to field doesnt exist in the '.get_class($this)
            );
        }
        return true;
    }

    /**
     * to ignore the null values when update
     * and the benefits is update just the data which sent in the request
     *
     * @param ObjectData|null $data
     * @return array
     */
    public function initializeForUpdate(?ObjectData $data=null){

        $arrayUpdate = [];
        foreach ($this->all() as $key=>$element){
            if(isset($element))
                $arrayUpdate[$key]=$element;
        }
        return $arrayUpdate;

    }

}

<?php

namespace App\Observers;

use App\Models\PopupLayoutInteraction;

class PopupLayoutInteractionObserver
{
    /**
     * Handle the PopupLayoutInteraction "created" event.
     *
     * @param  \App\Models\PopupLayoutInteraction  $popupLayoutInteraction
     * @return void
     */
    public function created(PopupLayoutInteraction $popupLayoutInteraction)
    {
        //TODO : here we should notify another part waiting this action
        // or maybe notify the owner of this popup
    }

    /**
     * Handle the PopupLayoutInteraction "updated" event.
     *
     * @param  \App\Models\PopupLayoutInteraction  $popupLayoutInteraction
     * @return void
     */
    public function updated(PopupLayoutInteraction $popupLayoutInteraction)
    {
        //
    }

    /**
     * Handle the PopupLayoutInteraction "deleted" event.
     *
     * @param  \App\Models\PopupLayoutInteraction  $popupLayoutInteraction
     * @return void
     */
    public function deleted(PopupLayoutInteraction $popupLayoutInteraction)
    {
        //
    }

    /**
     * Handle the PopupLayoutInteraction "restored" event.
     *
     * @param  \App\Models\PopupLayoutInteraction  $popupLayoutInteraction
     * @return void
     */
    public function restored(PopupLayoutInteraction $popupLayoutInteraction)
    {
        //
    }

    /**
     * Handle the PopupLayoutInteraction "force deleted" event.
     *
     * @param  \App\Models\PopupLayoutInteraction  $popupLayoutInteraction
     * @return void
     */
    public function forceDeleted(PopupLayoutInteraction $popupLayoutInteraction)
    {
        //
    }
}

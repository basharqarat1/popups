<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PopupButton extends Model
{
    use HasFactory;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'layout_id',
        'value',
        'bg_color',
        'click_action_types',
        'click_action_value',
        'height',
        'width',
        'x_axis',
        'y_axis',
    ];


    public function Layout()
    {
        return $this->belongsTo(Layout::class);
    }



}

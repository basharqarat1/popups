<?php

namespace App\Http\Services\Popup;


use App\Http\DTO\Popup\PopupButtonData;
use App\Http\DTO\Popup\PopupContentData;
use App\Http\DTO\Popup\PopupData;
use App\Http\DTO\Popup\PopupForm\PopupFormData;
use App\Http\DTO\Popup\PopupLayoutData;
use App\Http\DTO\Popup\PopupTargetedPageData;
use App\Http\Services\Popup\Button\ButtonService;
use App\Http\Services\Popup\Form\FormButtonService;
use App\Http\Services\Popup\Form\FormInputService;
use App\Http\Services\Popup\Form\FormService;
use App\Http\Services\Popup\Content\ContentService;
use App\Http\Services\Popup\Layout\LayoutService;
use App\Http\Services\Popup\targetedPage\TargetedPageService;
use App\Models\Layout;
use App\Models\Popup;

/**
 * here we can override the function in child classes if it have different logic inside it
 * or if each class of he children have different logic we should define the function as abstract
 *but i didnt see the diff for the popups types so i can do all logic here
 *
 * Class PopupAbstractClass
 * @package App\Http\Services\Popup
 */

abstract class PopupAbstractClass
{


    /**
     * @param PopupData $popupData
     */
    public function create(PopupData $popupData){
        $popup = $this->createPopup($popupData);
        $layout = $this->createPopupLayout($popupData->layout_data,$popup->id);
        foreach ($popupData->contents_data as $contentData){
           $this->createPopupContent($contentData,$layout->id);
        }
        foreach ($popupData->buttons_data as $buttonData){
            $this->createPopupButton($buttonData,$layout->id);
        }
        foreach ($popupData->form_data as $formData){
            $this->createPopupForm($formData,$layout->id);
        }
        foreach ($popupData->targeted_pages_data as $targetedPageData){
            $this->createPopupTargetedPage($targetedPageData,$popup->id);
        }
    }


    /**
     *
     * this will archive old layout and create new one to keep he interaction for users with variations of popup
     *
     * @param PopupData $popupData
     * @param Popup $popup
     */
    public function update(PopupData $popupData,Popup $popup){
        Layout::where('popup_id',$popup->id)->update([
            'is_archived' => true
        ]);
        $layout = $this->createPopupLayout($popupData->layout_data,$popup->id);
        foreach ($popupData->contents_data as $contentData){
            $this->createPopupContent($contentData,$layout->id);
        }
        foreach ($popupData->buttons_data as $buttonData){
            $this->createPopupButton($buttonData,$layout->id);
        }
        foreach ($popupData->form_data as $formData){
            $this->createPopupForm($formData,$layout->id);
        }
    }

    /**
     * @param PopupData $popupData
     * @return Popup
     */
    protected function createPopup(PopupData $popupData){
        return Popup::create($popupData->all());
    }

    /**
     * @param PopupLayoutData $popupLayoutData
     * @param int $popup_id
     * @return \App\Models\Layout
     */
    protected function createPopupLayout(PopupLayoutData $popupLayoutData,int $popup_id){
        return LayoutService::create($popupLayoutData,$popup_id);
    }

    /**
     * @param PopupContentData $popupContentData
     * @param int $layout_id
     * @return \App\Models\PopupContent
     */
    protected function createPopupContent(PopupContentData $popupContentData,int $layout_id){
        return ContentService::create($popupContentData,$layout_id);
    }

    /**
     * @param PopupButtonData $popupButtonData
     * @param int $layout_id
     * @return \App\Models\PopupButton
     */
    protected function createPopupButton(PopupButtonData $popupButtonData,int $layout_id){
        return ButtonService::create($popupButtonData,$layout_id);
    }

    /**
     * @param PopupFormData $popupFormData
     * @param int $layout_id
     * @return \App\Models\PopupForm
     */
    protected function createPopupForm(PopupFormData $popupFormData,int $layout_id){

        $popupForm = FormService::create($popupFormData,$layout_id);

        foreach ($popupFormData->form_inputs_data as $formInputData){
            FormInputService::create($formInputData,$popupForm->id);
        }
        foreach ($popupFormData->form_buttons_data as $formButtonData){
            FormButtonService::create($formButtonData,$popupForm->id);
        }

        return $popupForm;
    }


    /**
     * @param PopupTargetedPageData $targetedPageData
     * @param int $popup_id
     * @return \App\Models\PopupTargetedPage
     */
    protected function createPopupTargetedPage(PopupTargetedPageData $targetedPageData,int $popup_id){
        return TargetedPageService::create($targetedPageData,$popup_id);
    }


}

<?php


use App\Models\User;
use Illuminate\Support\Facades\Auth;

if(!function_exists('getUser')){

    /**
     * @param string $guard
     * @return User|null
     */
    function getUser($guard='sso'){
        return Auth::guard($guard)->user();

    }
}



if(!function_exists('baseRoute')){


    function baseRoute(){
        return url('/');

    }
}


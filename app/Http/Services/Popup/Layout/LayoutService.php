<?php


namespace App\Http\Services\Popup\Layout;


use App\Http\DTO\Popup\PopupLayoutData;
use App\Models\Layout;

class LayoutService
{

    /**
     * @param PopupLayoutData $layoutData
     * @param int $popup_id
     * @return  Layout
     */
    public static function create(PopupLayoutData $layoutData,int $popup_id){
        return Layout::create(
            array_merge($layoutData->all(),['popup_id'=>$popup_id])
        );
    }

}

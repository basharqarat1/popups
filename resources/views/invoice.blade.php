<!DOCTYPE html>
<html>
<head>
    <title>Hi</title>
</head>
<style>
    *{
        font-size: 15px;
    }
    body{
        margin-top: 100px;
        padding: 0px 65px;
    }

    .header{
        /*display: flex;*/
        /*justify-content: space-between;*/
    }

    .title > h1 {
        font-size: 25px;
    }

    .info{
        /*display: flex;*/
        /*justify-content: space-between;*/
    }

    .invoice-info{
        display: flex;
        flex-direction: column;
        gap: 45px;
    }

    .info-title{
        font-weight: bold;
    }

    .client-info{
        display: flex;
        justify-content: space-between;
    }

    .client-info > div{
        display: flex;
        flex-direction: column;
        width: 150px;
    }

    table {
        margin-top: 20px;
    }

    th{
        font-weight: normal;
    }

    th {
        text-align: left;
    }

    table{
        width: 100%;
    }

    table, th, td {
        border-bottom:1px solid black;
        border-collapse: collapse;
    }

    table > tr:last-child td{
        border-bottom:0;
    }

    .result{
        border-top: 1px solid;
        margin-top: 25px;
        float: right;
        width: 65%;
        padding: 10px 0px;
        border-bottom: 1px solid;
    }

    .result > div{
        display: flex;
        justify-content: space-between;
    }

    .endsumme-title,.endsumme-number{
        font-weight: bold;
    }
    /*.header > div{*/
    /*    width: 40%;*/
    /*}*/
</style>
<body>
<div class="header">
    <div style="float: left;width: 50%;">
        <p>info@zounder.com</p>
        <h1>Deutschland</h1>
    </div>
    <div style="float: right;width: 15%;">
        <p>info@zounder.com</p>
        <p>Phone: +49017684009725</p>
        <p>Fax: ....</p>
    </div>
</div>


<div class="content" style="clear: both;">
    <div class="title">
        <h1>Rechnung {{$invoiceNumber}}</h1>
    </div>

    <div class="info">
        <div class="invoice-info" style="float: left;width:50%">
            <div>
                Kunden-Nr.:  &nbsp &nbsp {{$customerNumber}}
            </div>
            <div>
                <span class="info-title">Lieferadresse:</span> Musterow GmbH | Moskau
            </div>
        </div>

        <div class="client-info" style="float: right;width:15%">
            <div>
                <span>Belegdatum:</span>
                <span>Bearbeiter:</span>
                <span>E-Mail:</span>
            </div>

            <div>
                <span>{{$date}}</span>
                <span>Manu Kellner</span>
                <span>info@zounder.com</span>
            </div>
        </div>

    </div>

    <div style="padding-top: 15px;clear: both">
        <table>
            <tr>
                <th>Pos.</th>
                <th>Art-Nr.</th>
                <th>Bezeichnung </th>
                <th>Menge</th>
                <th>Einheit</th>
                <th>Preis/Einh €</th>
                <th>Gesamt €</th>

            </tr>

            <tr>
                <td>1</td>
                <td>Z101</td> {{--this is article number--}}
                <td>
                    Zundor inside {{--Article name--}}
                </td>
                <td>1,00</td>
                <td>g</td>
                <td>{{$price}},00</td>
                <td>{{$price}},00</td>
            </tr>
        </table>
    </div>



    <div class="result">
        <div>
            <span >Positionen netto</span>
            <span>{{$price}},00 €</span>
        </div>
        <div>
            <span >Positionen USt. 0,00% auf {{$price}},00 €</span>
            <span>0,00 €</span>
        </div>
        <div>
            <span class="endsumme-title">Endsumme</span>
            <span class="endsumme-number">{{$price}},00 €</span>
        </div>
    </div>

</div>




</body>
</html>

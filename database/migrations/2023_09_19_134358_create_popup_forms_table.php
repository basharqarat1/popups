<?php

use App\Models\Layout;
use App\Models\Popup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('popup_forms', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('layout_id');
            $table->foreign('layout_id')->references('id')->on('layouts')->onDelete('cascade');


            $table->string('action_url');
            $table->integer('height');
            $table->integer('width');
            //for display the element in the screen when need the position
            $table->float('x_axis');
            $table->float('y_axis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('popup_forms');
    }
};

<?php

namespace App\Http\Requests\Popup;

use App\Http\Traits\ResponseValidationFormRequest;
use App\Models\Popup;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePopupRequest extends FormRequest
{

    use ResponseValidationFormRequest;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {

        return array_merge(
          $this->getPopupValidationRules(),
          $this->getLayoutValidationRules(),
          $this->getPopupContentValidationRules(),
          $this->getPopupButtonsValidationRules(),
          $this->getPopupFormsValidationRules(),
          $this->getPopupTargetedPages()
        );

    }


    protected function getPopupValidationRules(){
        return [
            'popup_title' => ['required','string'],
            'popup_type' => ['required',Rule::in(Popup::TYPES)],
        ];
    }


    protected function getLayoutValidationRules(){
        return [
            'layout_height' => ['required','numeric'],
            'layout_width' => ['required','numeric'],
            'layout_bg_color' => ['required','string'],
            'layout_border_type' => ['nullable',Rule::in(config('panel.styles.border_types'))],
            'layout_border_color' => ['nullable','string'],
            'layout_border_width' => ['nullable','numeric'],
            'layout_shadow' => ['nullable','string'],
            'layout_border_radius' => ['nullable','numeric'],
        ];
    }


    protected function getPopupContentValidationRules(){
        return [
            'popup_contents' => ['nullable','array'],
            'popup_contents.*.contentt' => ['required_with:popup_contents','string'],
            'popup_contents.*.height' => ['required_with:popup_contents','numeric'],
            'popup_contents.*.width' => ['required_with:popup_contents','numeric'],
            'popup_contents.*.x_axis' => ['required_with:popup_contents','numeric'],
            'popup_contents.*.y_axis' => ['required_with:popup_contents','numeric'],
        ];
    }


    protected function getPopupButtonsValidationRules(){
        return [
            'popup_buttons' => ['nullable','array'],
            'popup_buttons.*.value' => ['required_with:popup_buttons','string'],
            'popup_buttons.*.bg_color' => ['required_with:popup_buttons','string'],
            'popup_buttons.*.click_action_types' => ['required_with:popup_buttons',Rule::in([
                config('panel.button_actions_types.link'),
                config('panel.button_actions_types.close'),
            ])],
            'popup_buttons.*.click_action_value' => ['required_with:popup_buttons','url'],
            'popup_buttons.*.height' => ['required_with:popup_buttons','numeric'],
            'popup_buttons.*.width' => ['required_with:popup_buttons','numeric'],
            'popup_buttons.*.x_axis' => ['required_with:popup_buttons','numeric'],
            'popup_buttons.*.y_axis' => ['required_with:popup_buttons','numeric'],
        ];
    }

    protected function getPopupFormsValidationRules(){
        return [
            'popup_forms' => ['nullable','array'],
            'popup_forms.*.action_url' => ['required_with:popup_forms','url'],
            'popup_forms.*.height' => ['required_with:popup_forms','numeric'],
            'popup_forms.*.width' => ['required_with:popup_forms','numeric'],
            'popup_forms.*.x_axis' => ['required_with:popup_forms','numeric'],
            'popup_forms.*.y_axis' => ['required_with:popup_forms','numeric'],

            'popup_forms.*.inputs' => ['required_with:popup_forms','array'],
            'popup_forms.*.inputs.*.type' => ['required_with:popup_forms.*.inputs',Rule::in([
                config('panel.input_types.text'),
                config('panel.input_types.number'),
                config('panel.input_types.email'),
            ])],
            'popup_forms.*.inputs.*.is_required' => ['required_with:popup_forms.*.inputs','boolean'],
            'popup_forms.*.inputs.*.width' => ['required_with:popup_forms.*.inputs','numeric'],
            'popup_forms.*.inputs.*.x_axis' => ['required_with:popup_forms.*.inputs','numeric'],
            'popup_forms.*.inputs.*.y_axis' => ['required_with:popup_forms.*.inputs','numeric'],

            'popup_forms.*.buttons' => ['required_with:popup_forms','array'],
            'popup_forms.*.buttons.*.value' => ['required_with:popup_forms.*.buttons','string'],
            'popup_forms.*.buttons.*.bg_color' => ['required_with:popup_forms.*.buttons','string'],
            'popup_forms.*.buttons.*.click_action_types' => ['required_with:popup_forms.*.buttons',Rule::in([
                config('panel.button_actions_types.link'),
                config('panel.button_actions_types.submit'),
            ])],
            'popup_forms.*.buttons.*.click_action_value' => ['required_with:popup_forms.*.buttons','url'],
            'popup_forms.*.buttons.*.height' => ['required_with:popup_forms.*.buttons','numeric'],
            'popup_forms.*.buttons.*.width' => ['required_with:popup_forms.*.buttons','numeric'],
            'popup_forms.*.buttons.*.x_axis' => ['required_with:popup_forms.*.buttons','numeric'],
            'popup_forms.*.buttons.*.y_axis' => ['required_with:popup_forms.*.buttons','numeric'],
        ];
    }

    protected function getPopupTargetedPages(){
        return [
          'pages_identifiers' => ['required','array'],
          'pages_identifiers.*' => ['required',Rule::in(config('panel.page_identifiers'))],
        ];
    }


}

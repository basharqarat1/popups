<?php

use App\Models\Layout;
use App\Models\Popup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('popup_form_inputs', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('popup_form_id');
            $table->foreign('popup_form_id')->references('id')->on('popup_forms')->onDelete('cascade');


            $table->enum('type',[
                config('panel.input_types.text'),
                config('panel.input_types.number'),
                config('panel.input_types.email'),
            ]);
            $table->boolean('is_required')->default(false);
//            $table->integer('order')->default(0);
            $table->integer('width');
            //for display the element in the screen when need the position bu here its inside the form
            $table->float('x_axis');
            $table->float('y_axis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('popup_form_inputs');
    }
};

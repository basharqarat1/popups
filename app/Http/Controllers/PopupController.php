<?php

namespace App\Http\Controllers;


use App\Http\DTO\Popup\PopupData;
use App\Http\Requests\Popup\StorePopupRequest;
use App\Http\Requests\Popup\UpdatePopupRequest;
use App\Http\Resources\PopupResource;
use App\Http\Services\ApiResponse\ApiResponseClass;
use App\Http\Services\Popup\PopupFactory;
use App\Models\Popup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

/**
 * Class PopupController
 * I have considered that this project belongs for one user so i didnt applied the authorization and authentication
 *
 * @package App\Http\Controllers\User
 */
class PopupController extends Controller
{

    /**
     * we can use this function to return the popups in defined page when frontend visit it
     * then can send the identifier for this page or we can make function to return all popups if the count of them is not huge
     * then frontend can cache them without calling API
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPopupsByTargetPageIdentifier(Request $request){
        $popups = Cache::rememberForever("popups_for_page_{$request->page_identifier}", function () use ($request) {
            return $popups = Popup::wherehas('TargetedPages',function ($query)use ($request){
                return $query->where('page_identifier',$request->page_identifier);
            })
                ->with([
                    'Layouts'=>function($query){
                        return $query->archived()
                            ->with([
                                'PopupContents',
                                'PopupButtons',
                                'PopupForms.Inputs',
                                'PopupForms.Buttons',
                            ]);
                    },
                ])
                ->get();
        });

        return ApiResponseClass::successResponse(PopupResource::collection($popups));
    }

    /**
     * in store function i have collected al the popup data in one method to save he consistency of the database
     * and to reduce requests count
     * or maybe we can to divide it to store each thing separately
     *
     * @param StorePopupRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ErrorMsgException
     */
    public function store(StorePopupRequest $request){
        DB::beginTransaction();
        $popupData = PopupData::fromRequest($request);

        $popupClass = PopupFactory::create($popupData->type);
        $popupClass->create($popupData);
        DB::commit();

        return ApiResponseClass::successMsgResponse();

    }

    /**
     * the update will create new instance for layout ,styles and forms... => is_archived will be true in old instance
     * to keep the users interaction with variations of old layouts and can compare
     *
     * @param UpdatePopupRequest $request
     * @param $popup_id
     */
    public function update(UpdatePopupRequest $request,$popup_id){
        DB::beginTransaction();

        //i have used override for the error of findOrFail to return json response
        $popup = Popup::findOrFail($popup_id);
        $popupData = PopupData::forUpdate($popup,$request);

        $popupClass = PopupFactory::create($popupData->type);
        $popupClass->update($popupData,$popup);
        DB::commit();

        return ApiResponseClass::successMsgResponse();

    }




}

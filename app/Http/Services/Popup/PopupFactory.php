<?php

namespace App\Http\Services\Popup;

use App\Exceptions\ErrorMsgException;
use App\Models\Popup;

final class PopupFactory
{

    private static $paths = [
      Popup::FULL_SCREEN_OVERLAYS => FullScreenOverlayPopupClass::class,
      Popup::SLIDE_IN => SlideInPopupClass::class,
      Popup::EXIT_INTENT => ExitIntentPopupClass::class,
    ];


    /**
     * @param string $reserveType
     * @return PopupAbstractClass
     * @throws ErrorMsgException
     */
    public static function create($popupType):PopupAbstractClass{

        if(!key_exists($popupType,self::$paths))
            throw new ErrorMsgException('trying to declare invalid class type ');

        $classPath = self::$paths[$popupType];
        if(class_exists($classPath)){
            return new $classPath();
        }
        throw new ErrorMsgException('trying to declare invalid class type ');
    }





}

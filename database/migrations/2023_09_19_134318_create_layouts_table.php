<?php

use App\Models\Layout;
use App\Models\Popup;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('layouts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('popup_id');
            $table->foreign('popup_id')->references('id')->on('popups')->onDelete('cascade');

            $table->enum('type',config('panel.targeted_device_types'))
                ->default(config('panel.targeted_device_types.desktop'));//for multi platforms but we will work wih desktop
            $table->integer('height');
            $table->integer('width');
            $table->string('bg_color');
            $table->enum('border_type',config('panel.styles.border_types'))
                ->default(config('panel.styles.border_types.none'));
            $table->string('border_color')->nullable();
            $table->integer('border_width')->nullable();
            $table->string('shadow')->nullable();
            $table->integer('border_radius')->nullable();
            // I will use this column to determine the content is most effective.
            //when i update a popup style then i will save the old one here as archive
            $table->boolean('is_archived')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('layouts');
    }
};
